import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AboutComponent } from './about/about.component';
import { ClientComponent } from './client/client.component';
import { FactsComponent } from './facts/facts.component';
import { TeamComponent } from './team/team.component';
import { FeaturesComponent } from './features/features.component';
import { ProjectComponent } from './project/project.component';
import { ModelComponent } from './model/model.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavigationComponent,
    AboutComponent,
    ClientComponent,
    FactsComponent,
    TeamComponent,
    FeaturesComponent,
    ProjectComponent,
    ModelComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
